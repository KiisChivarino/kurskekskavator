<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <title>Мини-экскаватор в Курске</title>
    <meta name="description" content="Здесь можно заказать услуги мини-экскаватора в Курске и Курской области, узнать цены">
	<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
<!--

Sprint Template

http://www.templatemo.com/tm-401-sprint

-->
    <meta name="viewport" content="width=device-width">

    <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet">-->

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/normalize.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/templatemo_misc.css">
    <link rel="stylesheet" href="css/templatemo_style.css">
	<link rel="stylesheet" href="css/jquery.fancybox.min.css" >
	<!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">-->
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>

</head>
<body>
    <!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
    <div id="front">
        <div class="site-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div id="templatemo_logo">
                            <h1><a href="#contact">Заказ <br> мини-экскаватора<br/>89611959207</a></h1>
                        </div> <!-- /.logo -->
                    </div> <!-- /.col-md-4 -->
                    <div class="col-md-9 col-sm-6 col-xs-6">
                        <a href="#" class="toggle-menu"><i class="fa fa-bars"></i></a>
                        <div class="main-menu">
                            <ul>
                                <li><a href="#front">Начало</a></li>
                                <li><a href="#services">Услуги</a></li>
								<!--<li><a href="#product-promotion">Статьи</a></li>-->
                                <li><a href="#products">Работы</a></li>
                                <li><a href="#contact">Контакты</a></li>
                            </ul>
                        </div> <!-- /.main-menu -->
                    </div> <!-- /.col-md-8 -->
                </div> <!-- /.row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="responsive">
                            <div class="main-menu">
                                <ul>
                                    <li><a href="#front">Начало</a></li>
                                    <li><a href="#services">Услуги</a></li>
                                    <!--<li><a href="#product-promotion">Статьи/a></li>-->
									<li><a href="#products">Работы</a></li>
                                    <li><a href="#contact">Контакты</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- /.container -->
        </div> <!-- /.site-header -->
    </div> <!-- /#front -->

    <div class="site-slider">
        <ul class="bxslider">
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="slider-caption">
								<div class="responsive">
                                <h2><a href="#services">Услуги мини-экскаватора в Курске и Курской области</a></h2>
								</div>
							</div>
                        </div>
                    </div>
                </div>
				<img src="<?PHP echo $SERVER['DOCUMENT_ROOT']?>/images/slider/slide1.jpg" alt="slider image 1">
            </li>
            <li>
                <div class="container caption-wrapper">
                    <div class="slider-caption">
                        <h2><a href="#contact">без посредников</a></h2>
                    </div>
                </div>
				<img src="<?PHP echo $SERVER['DOCUMENT_ROOT']?>/images/slider/slide2.jpg" alt="slider image 2">
            </li>
            <li>
				<div class="container">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="slider-caption">
                                <h2>все виды работ</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <img src="<?PHP echo $SERVER['DOCUMENT_ROOT']?>/images/slider/slide3.jpg" alt="slider image 3">
            </li>
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="slider-caption">
                                <h2>Выезжаем в районы</h2>
                            </div>
                        </div>
                    </div>
                </div>
				<img src="<?PHP echo $SERVER['DOCUMENT_ROOT']?>/images/slider/slide4.jpg" alt="slider image 4">
            </li>
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="slider-caption">
                                <h2>Большой опыт работы</h2>
                            </div>
                        </div>
                    </div>
                </div>
				<img src="<?PHP echo $SERVER['DOCUMENT_ROOT']?>/images/slider/slide5.jpg" alt="slider image 5">
            </li>
        </ul> <!-- /.bxslider -->
        <!--<div class="bx-thumbnail-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="bx-pager">
                            <a data-slide-index="0" href=""><img src="images/slider/thumb1.jpg" alt="image 1" /></a>
                            <a data-slide-index="1" href=""><img src="images/slider/thumb2.jpg" alt="image 2" /></a>
                            <a data-slide-index="2" href=""><img src="images/slider/thumb3.jpg" alt="image 3" /></a>
                            <a data-slide-index="3" href=""><img src="images/slider/thumb4.jpg" alt="image 4" /></a>
                            <a data-slide-index="4" href=""><img src="images/slider/thumb5.jpg" alt="image 5" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
    </div> <!-- /.site-slider -->

    <div id="services" class="content-section">
        <div class="container">
			<div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="section-title">Услуги</h1>
                </div> <!-- /.col-md-12 -->
				<div class="col-md-offset-2 col-md-8 text-center bigger-text" style="margin-bottom:0">
								<p>Все виды земляных работ!</p>
						</div>
            </div> <!-- /.row -->
			
			<div class="row">
				<div class="col-md-1 hidden-sm"></div>
				<div class="col-md-4 col-sm-6">
					<div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6"><span class="service-icon first"></span></div>
							<div class="col-md-10 col-sm-6">
									<h3>Траншеи</h3>
									<p>Копка траншей под фундамент, газ, воду, кабель, сливные ямы и др.</p>
							</div>
						</div>
					</div>
                </div> <!-- /.col-md-3 -->
				<div class="col-md-3 hidden-sm"></div>
                <div class="col-md-4 col-sm-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6"><span class="service-icon second"></span></div>
							<div class="col-md-10 col-sm-6">
								<h3>Фундаменты</h3>
								<p>Копка фундаментов</p>
							</div>
						</div>
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<!--<div class="col-md-1 hidden-sm"></div>-->
			</div> <!-- /.row -->
            <div class="row">
				<div class="col-md-1 hidden-sm"></div>
				<div class="col-md-4 col-sm-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6"> <span class="service-icon third"></span></div>
							<div class="col-md-10 col-sm-6">
								<h3>Сливные ямы</h3>
								<p>Копка сливных ям под канализацию, туалет, септик любого объема и глубины</p>
							</div>
						</div>
                        <!--<p>Sprint is free responsive web template using HTML5 CSS3 and Bootstrap framework. Feel free to download, modify and use it for your site.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<div class="col-md-3 hidden-sm"></div>
                <div class="col-md-4 col-sm-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6"><span class="service-icon fourth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h3>Водоемы</h3>
								<p>Копка бассейнов, продов и водоемов</p>
							</div>
						</div>
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<!--<div class="col-md-1 hidden-sm"></div>-->
			</div>
			<div class="row">
				<div class="col-md-1 hidden-sm"></div>
                <div class="col-md-4 col-sm-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6"><span class="service-icon fifth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h3>Подвалы</h3>
								<p>Копка подвалов</p>
							</div>
						</div>
                        <!--<p>Копаем траншеи под фундамент,газ,воду,кабель,а также сливные ямы.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<div class="col-md-3 hidden-sm"></div>
                <div class="col-md-4 col-sm-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6"><span class="service-icon sixth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h3>Погреба</h3>
								<p>Копка погребов</p>
							</div>
						</div>
                        <!--<p>Etiam porttitor nibh et felis molestie fermentum. Ut quis diam porttitor, dictum dolor in, volutpat nulla. Phasellus egestas eu lacus eu pharetra.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<!--<div class="col-md-1 hidden-sm"></div>-->
			</div> <!-- /.row -->
			
			<div class="row">
				<div class="col-md-1 hidden-sm"></div>
                <div class="col-md-4 col-sm-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6"><span class="service-icon fifth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h3>Обустройство скважин</h3>
								<p></p>
							</div>
						</div>
                        <!--<p>Копаем траншеи под фундамент,газ,воду,кабель,а также сливные ямы.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<div class="col-md-3 hidden-sm"></div>
                <div class="col-md-4 col-sm-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6"><span class="service-icon sixth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h3>Колодцы</h3>
								<p>Копка колодцев</p>
							</div>
						</div>
                        <!--<p>Etiam porttitor nibh et felis molestie fermentum. Ut quis diam porttitor, dictum dolor in, volutpat nulla. Phasellus egestas eu lacus eu pharetra.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<!--<div class="col-md-1 hidden-sm"></div>-->
			</div> <!-- /.row -->
			
			<div class="row">
				<div class="col-md-1 hidden-sm"></div>
                <div class="col-md-4 col-sm-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6"><span class="service-icon fifth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h3>Котлованы</h3>
								<p>Копка котлованов</p>
							</div>
						</div>
                        <!--<p>Копаем траншеи под фундамент,газ,воду,кабель,а также сливные ямы.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<div class="col-md-3 hidden-sm"></div>
                <div class="col-md-4 col-sm-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6"><span class="service-icon sixth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h3>Закопка траншей</h3>
							</div>
						</div>
                        <!--<p>Etiam porttitor nibh et felis molestie fermentum. Ut quis diam porttitor, dictum dolor in, volutpat nulla. Phasellus egestas eu lacus eu pharetra.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<!--<div class="col-md-1 hidden-sm"></div>-->
			</div> <!-- /.row -->
			
			<div class="row">
				<div class="col-md-1 hidden-sm"></div>
                <div class="col-md-4 col-sm-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6"><span class="service-icon seventh"></span></div>
							<div class="col-md-10 col-sm-6">
								<h3>Планировка территории</h3>
							</div>
						</div>
                        <!--<p>Sprint is free responsive web template using HTML5 CSS3 and Bootstrap framework. Feel free to download, modify and use it for your site.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<div class="col-md-3 hidden-sm"></div>
                <div class="col-md-4 col-sm-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6"><span class="service-icon eighth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h3>Дополнительные услуги</h3>
								<p>Также есть манипулятор стрела 7т и бригада каменщиков: кладут красиво и аккуратно без вредных привычек</p>
							</div>
						</div>
                </div> <!-- /.col-md-3 -->
				<!--<div class="col-md-1 hidden-sm"></div>-->
			</div> <!-- /.row -->
			<div class="row">
				<div class="col-md-offset-2 col-md-8 text-center bigger-text">
                    <p>Есть предложения по другим видам работ? <a href="#contact">Свяжитесь с нами!</a></p>
					<p>Работаем на всей территории Курской области</p>
                </div>
			</div>
        </div> <!-- /.container -->
    </div> <!-- /#services -->

	
	<div id="product-promotion" class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="section-title">Технические характеристики</h1>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
            <div class="row">                
				<div class="item-large">
					<div class="col-md-12 col-sm-12">
						<div class="item-header">
							<h2 class="pull-left">JCB 8018 на резиновом ходу</h2>
							<!--<span class="pull-right">Rate: <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i></span>-->
							<div class="clearfix"></div>
						</div> <!-- /.item-header -->
					</div>
					<div class="col-md-6 col-sm-6">
						<img src="/images/tech.jpg" alt="Мини-экскаватор технические характеристики">
					</div>
					<div class="tech-content">
						<div class="col-md-6 col-sm-6">
							<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 tech title">Ковши:</div> <div class="col-md-6 col-sm-6 col-xs-6 tech value"><strong>30, 40, 50, 60, 70 (см)</strong></div>
							</div>
							<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 tech title">Глубина копания</div> <div class="col-md-6 col-sm-6 col-xs-6 tech value"><strong>3 м</strong></div>
							</div>
							<div class="row">
							<div class="col-md-12 col-sm-12 tech title" style="text-align: center; margin-top: 15px;">Габаритные размеры экскаватора:</div>
							</div>
							<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 tech title">ширина</div> <div class="col-md-6 col-sm-6 col-xs-6 tech value"><strong>1,6 м</strong></div>
							</div>
							<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 tech title">высота</div> <div class="col-md-6 col-sm-6 col-xs-6 tech value"><strong>2,6 м</strong></div>
							</div>
							<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 tech title">длина</div> <div class="col-md-6 col-sm-6 col-xs-6 tech value"><strong>3,5 м</strong></div>
							</div>
							<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 tech title">планировочный нож</div> <div class="col-md-6 col-sm-6 col-xs-6 tech value"><strong>2 м</strong></div>
							</div>
							<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 tech title">Вес:</div> <div class="col-md-6 col-sm-6 col-xs-6 tech value"><strong>3000 кг</strong></div>
							</div>
						</div>
					</div>
				</div> <!-- /.item-large -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /#products -->
	
	
    <div id="product-promotion" class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="section-title">Статьи и новости</h1>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
            <div class="row">
                <div class="col-md-2 col-sm-3">
                    <div class="item-small left">
                        <img src="/images/promotion/promotion5.jpg" alt="Product 1">
                        <h4>Траншеи под сливные ямы</h4>
                    </div> <!-- /.item-small -->
                </div> <!-- /.col-md-2 -->
                <div class="col-md-8 col-sm-6">
					<div class="item-large">
                        <img src="/images/promotion/promotion1.jpg" alt="Product 2">
                        <div class="item-large-content">
                            <div class="item-header">
                                <h2 class="pull-left">Рытье траншей</h2>
                                <!--<span class="pull-right">Rate: <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i></span>-->
                                <div class="clearfix"></div>
                            </div> <!-- /.item-header -->
                            <p>Рытье траншей — наиболее востребованный вид экскаваторных работ. Траншеи роют для прокладки водопровода, канализации, отопления, кабеля и т. д.</p>
							<p>В каждом случае глубина, ширина и длина траншеи будут разными, но общие принципы копания остаются неизменными. </p>
							<p>Цикл работы экскаватора включает несколько этапов:</p>
<ul>
<li>резание грунта</li>
<li>разворот платформы</li>
<li>разгрузка ковша</li>
<li>обратный поворот платформы</li>
<li>установка ковша для повторения цикла</li>
</ul>
<p>Прежде всего, необходимо очистить забой (площадку, на которой выполняется работа, расположен экскаватор и дополнительная техника) от мусора, крупных камней, пней и любых других лишних объектов.</p>

<p>Если грунт необходимо транспортировать на большое расстояние, содержимое ковша высыпают в кузов грузовых машин. Считается оптимальной ситуация, когда в кузове грузовика помещается 3-7 полных ковшей. Наполнение кузова производится сзади или сбоку, перемещение ковша над кабиной запрещено. Заполнение должно производиться равномерно, но разравнивать содержимое кузова ковшом не разрешается.</p>
                        </div> <!-- /.item-large-content -->
                    </div> <!-- /.item-large -->
					 <div class="item-large" style="display:none">
                        <img src="/images/promotion/promotion2.jpg" alt="Product 2">
                        <div class="item-large-content">
                            <div class="item-header">
                                <h2 class="pull-left">2-Траншеи под газ</h2>
                                <!--<span class="pull-right">Rate: <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i></span>-->
                                <div class="clearfix"></div>
                            </div> <!-- /.item-header -->
                            <p>Вырыть траншею под фундамент – только на первый взгляд кажется, что нет ничего проще, но это обманчивое впечатление стороннего наблюдателя. Перед рытьем котлована или копкой траншеи под фундамент любой архитектор уделяет больше внимания предварительным расчетам нагрузки и характеристикам грунтов, чем разработке проекта и его сопровождению. Основание дома начинается не с разметки, хотя и без нее не обойтись, а с более важных показателей грунта. Также, важен учет таких параметров, как форма, глубина и ширина траншей под фундамент дома или дачи.</p>
                        </div> <!-- /.item-large-content -->
                    </div> <!-- /.item-large -->
					 <div class="item-large" style="display:none">
                        <img src="/images/promotion/promotion3.jpg" alt="Product 2">
                        <div class="item-large-content">
                            <div class="item-header">
                                <h2 class="pull-left">3-Траншеи под воду</h2>
                                <!--<span class="pull-right">Rate: <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i></span>-->
                                <div class="clearfix"></div>
                            </div> <!-- /.item-header -->
                            <p>Вырыть траншею под фундамент – только на первый взгляд кажется, что нет ничего проще, но это обманчивое впечатление стороннего наблюдателя. Перед рытьем котлована или копкой траншеи под фундамент любой архитектор уделяет больше внимания предварительным расчетам нагрузки и характеристикам грунтов, чем разработке проекта и его сопровождению. Основание дома начинается не с разметки, хотя и без нее не обойтись, а с более важных показателей грунта. Также, важен учет таких параметров, как форма, глубина и ширина траншей под фундамент дома или дачи.</p>
                        </div> <!-- /.item-large-content -->
                    </div> <!-- /.item-large -->
					 <div class="item-large" style="display:none">
                        <img src="/images/promotion/promotion4.jpg" alt="Product 2">
                        <div class="item-large-content">
                            <div class="item-header">
                                <h2 class="pull-left">4-Траншеи под кабель</h2>
                                <!--<span class="pull-right">Rate: <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i></span>-->
                                <div class="clearfix"></div>
                            </div> <!-- /.item-header -->
                            <p>Вырыть траншею под фундамент – только на первый взгляд кажется, что нет ничего проще, но это обманчивое впечатление стороннего наблюдателя. Перед рытьем котлована или копкой траншеи под фундамент любой архитектор уделяет больше внимания предварительным расчетам нагрузки и характеристикам грунтов, чем разработке проекта и его сопровождению. Основание дома начинается не с разметки, хотя и без нее не обойтись, а с более важных показателей грунта. Также, важен учет таких параметров, как форма, глубина и ширина траншей под фундамент дома или дачи.</p>
                        </div> <!-- /.item-large-content -->
                    </div> <!-- /.item-large -->
					 <div class="item-large" style="display:none">
                        <img src="/images/promotion/promotion5.jpg" alt="Product 2">
                        <div class="item-large-content">
                            <div class="item-header">
                                <h2 class="pull-left">5-Траншеи под сливные ямы</h2>
                                <!--<span class="pull-right">Rate: <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i></span>-->
                                <div class="clearfix"></div>
                            </div> <!-- /.item-header -->
                            <p>Вырыть траншею под фундамент – только на первый взгляд кажется, что нет ничего проще, но это обманчивое впечатление стороннего наблюдателя. Перед рытьем котлована или копкой траншеи под фундамент любой архитектор уделяет больше внимания предварительным расчетам нагрузки и характеристикам грунтов, чем разработке проекта и его сопровождению. Основание дома начинается не с разметки, хотя и без нее не обойтись, а с более важных показателей грунта. Также, важен учет таких параметров, как форма, глубина и ширина траншей под фундамент дома или дачи.</p>
                        </div> <!-- /.item-large-content -->
                    </div> <!-- /.item-large -->
                </div> <!-- /.col-md-8 -->
                <div class="col-md-2 col-sm-3">
                    <div class="item-small right">
                        <img src="/images/promotion/promotion2.jpg" alt="Product 3">
                        <h4>Траншеи под газ</h4>
                    </div> <!-- /.item-small -->
                </div> <!-- /.col-md-2 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /#product-promotion -->
    <div id="products" class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="section-title">Примеры работ</h1>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_1.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div>--> <!-- /.overlay -->
                            <img src="/images/products/product1.jpg" alt="">
                        </div> <!-- /.item-thumb -->
                        <!--<h3>Технические пояснения</h3>-->
                        <!--<span>Price: <em class="text-muted">$260.00</em> - <em class="price">$180.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_3.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div> --><!-- /.overlay -->
							<a data-fancybox="gallery" href="/images/products/product2.jpg">
								<img src="/images/products/product2.jpg" alt="">
							</a>
                        </div> <!-- /.item-thumb -->
                        <!--<h3>Технические пояснения</h3>-->
                        <!--<span>Price: <em class="text-muted">$320.00</em> - <em class="price">$240.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_2.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div> --><!-- /.overlay -->
                            <a data-fancybox="gallery" href="/images/products/product3.jpg">
								<img src="/images/products/product3.jpg" alt="">
							</a>
							
                        </div> <!-- /.item-thumb -->
                        <!--<h3>Технические пояснения</h3>-->
                        <!--<span>Price: <em class="text-muted">$480.00</em> - <em class="price">$340.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_1.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div> --><!-- /.overlay -->
                            <a data-fancybox="gallery" href="/images/products/product4.jpg">
								<img src="/images/products/product4.jpg" alt="">
							</a>
                        </div> <!-- /.item-thumb -->
                        <!--<h3>Технические пояснения</h3>-->
                        <!--<span>Price: <em class="text-muted">$260.00</em> - <em class="price">$140.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_3.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div> --><!-- /.overlay -->
							<a data-fancybox="gallery" href="/images/products/product5.jpg">
								<img src="/images/products/product5.jpg" alt="">
							</a>
                        </div> <!-- /.item-thumb -->
                        <!--<h3>Технические пояснения</h3>-->
                        <!--<span>Price: <em class="text-muted">$400.00</em> - <em class="price">$260.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_1.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div> --><!-- /.overlay -->
							<a data-fancybox="gallery" href="/images/products/product6.jpg">
								<img src="/images/products/product6.jpg" alt="">
							</a>
                        </div> <!-- /.item-thumb -->
                        <!--<h3>Технические пояснения</h3>-->
                        <!--<span>Price: <em class="text-muted">$470.00</em> - <em class="price">$330.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_2.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div> --><!-- /.overlay -->
							<a data-fancybox="gallery" href="/images/products/product7.jpg">
								<img src="/images/products/product7.jpg" alt="">
							</a>
                        </div> <!-- /.item-thumb -->
                        <!--<h3>Технические пояснения</h3>-->
                        <!--<span>Price: <em class="text-muted">$680.00</em> - <em class="price">$480.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_3.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div> --><!-- /.overlay -->
							<a data-fancybox="gallery" href="/images/products/product8.jpg">
								<img src="/images/products/product8.jpg" alt="">
							</a>
                        </div> <!-- /.item-thumb -->
                        <!--<h3>Технические пояснения</h3>-->
                        <!--<span>Price: <em class="text-muted">$820.00</em> - <em class="price">$660.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /#products -->
	
	
	 <div id="prices" class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="section-title">Цены</h1>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
            <div class="row">
                <div class="col-md-2 hidden-sm"></div>
				<div class="col-md-5 col-sm-6">
					<h4>Один метр траншеи</h4>
				</div>
				<div class="col-md-5 col-sm-6">
					<h3>150 р.</h3>
				</div>
				<div class="col-md-2 hidden-sm"></div>
            </div> <!-- /.row -->
			<div class="row">
				<div class="col-md-2 hidden-sm"></div>
                <div class="col-md-5 col-sm-6">
					<h4>Один кубический метр ямы</h4>
				</div>
				<div class="col-md-5 col-sm-6">
					<h3>250 р.</h3>
				</div>
				<div class="col-md-2 hidden-sm"></div>
            </div> <!-- /.row -->
			<div class="row">
				<div class="col-md-2 hidden-sm"></div>
                <div class="col-md-5 col-sm-6">
					<h4>Планировочные работы</h4>
				</div>
				<div class="col-md-5 col-sm-6">
					<h3>1300 р. в час</h3>
				</div>
				<div class="col-md-2 hidden-sm"></div>
				<div class="col-md-offset-0 col-md-12 text-center bigger-text">
                    <p>Стоимость минимального заказа - <span class="price-value">3000 р.</span> Услуги и топливо входят в стоимость заказа.</p>
                </div>
				<div class="col-md-2 hidden-sm"></div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /#products -->
	
	
    <div id="contact" class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="section-title">Контакты</h1>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center bigger-text">
                    <p>Меня зовут Виталий. Звоните в любое время или присылайте сообщения на электронный почтовый ящик</p>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div id="phone"><span>Телефон:</span><h2><a href="tel:89611959207">89611959207</a></h2></div>
					<div id="email"><span>Электронная почта:</span><h2><a href="mailto:vetal.sokolov2018@yandex.ru">vetal.sokolov2018@yandex.ru</a></h2></div>
					<!--<div id="map">
                    </div>-->
                </div> <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-6">
                    <form action= "post.php" method= "POST"> 
                    <div class="row contact-form">
                    
                        <fieldset class="col-md-6 col-sm-6">
                            <input id="name" type="text" name="name" placeholder="Имя">
                        </fieldset>
                        <fieldset class="col-md-6 col-sm-6">
                            <input type="email" name="email" id="email" placeholder="Email">
                        </fieldset>
                        <fieldset class="col-md-12">
                            <input type="text" name="subject" id="subject" placeholder="Тема">
                        </fieldset>
                        <fieldset class="col-md-12">
                            <textarea name="comments" id="comments" placeholder="Сообщение"></textarea>
                        </fieldset>
                        <fieldset class="col-md-12">
                            <input type="submit" name="send" value="Отправить" id="submit" class="button">
                        </fieldset>
						<? if($_GET['FormErrors']==='0'):?>
						<fieldset class="col-md-12">
						   <p style="color: green;font-size:14px;font-weight:bolder;">Сообщение успешно отправлено!</p>
                        </fieldset>
						<?elseif ($_GET['FormErrors']==='1'):?>
						<fieldset class="col-md-12">
						   <p style="color: red;font-size:14px;font-weight:bolder;">Сообщение не отправлено! Проверьте введенные данные.</p>
                        </fieldset>
						<?endif;?>
                    </div> <!-- /.contact-form -->
                    </form>
                </div> <!-- /.col-md-6 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /#products -->

    <div class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-6">
                    <span id="copyright">
                    	Copyright &copy; 2018 <a href="mailto:kiischivarino@yandex.ru">Created by Kiis-Chivarino</a>
                    </span>

            </div> <!-- /.col-md-6 -->
                <div class="col-md-4 col-sm-6">
                    <ul class="social">
                        <li><a href="#" class="fa fa-facebook"></a></li>
                        <li><a href="#" class="fa fa-twitter"></a></li>
                        <li><a href="#" class="fa fa-instagram"></a></li>
                        <li><a href="#" class="fa fa-linkedin"></a></li>
                        <li><a href="#" class="fa fa-rss"></a></li>
                    </ul>
                </div> <!-- /.col-md-6 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.site-footer -->

    
   <!--<script src="js/vendor/jquery-1.10.1.min.js"></script>
   <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
   -->
   <script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
   
	<script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing-1.3.js"></script>
	<script src="js/jquery.easing.js"></script>
    <script src="js/bootstrap.js"></script>
	<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    <!-- templatemo 401 sprint -->
</body>
</html>