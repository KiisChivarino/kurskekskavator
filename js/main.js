(function (window, $) {
	'use strict';

	// Cache document for fast access.
	var document = window.document;


	function mainSlider() {
		$('.bxslider').bxSlider({
			pagerCustom: '#bx-pager',
			mode: 'fade',
			nextText: '',
			prevText: ''
		});
	}

	mainSlider();



	var $links = $(".bx-wrapper .bx-controls-direction a, #bx-pager a");
	$links.click(function(){
	   $(".slider-caption").removeClass('animated fadeInLeft');
	   $(".slider-caption").addClass('animated fadeInLeft');
	});

	$(".bx-controls").addClass('container');
	$(".bx-next").addClass('fa fa-angle-right');
	$(".bx-prev").addClass('fa fa-angle-left');


	$('a.toggle-menu').click(function(){
        $('.responsive .main-menu').toggle();
        return false;
    });

    $('.responsive .main-menu a').click(function(){
        $('.responsive .main-menu').hide();

    });

    $('.main-menu').singlePageNav();
	
//	var dt = window.atob('fCBEZXNpZ246IDxhIHJlbD0ibm9mb2xsb3ciIGhyZWY9Imh0dHA6Ly93d3cudGVtcGxhdGVtby5jb20vdG0tNDAxLXNwcmludCIgdGFyZ2V0PSJfcGFyZW50Ij5TcHJpbnQ8L2E+'); 		// decode the string
//	var div = document.getElementById('copyright');

//	div.innerHTML += dt;
	$('.item-small.left').click(function(){
		var currItemLarge = 0;
		var leftImgSrc = '';
		var rightImgSrc = '';
		var leftTitleValue = '';
		var rightTitleValue = '';
		$('.item-large').each(function(ix,el){
			if(!$(el).is(':hidden')){
				currItemLarge = ix;
				return;
			}
		});
		if(currItemLarge==0){
			
			$('.item-large').first().hide();
			$('.item-large').last().show();
			
			leftImgSrc = $('.item-large').eq($('.item-large').length-2).children('img').first().attr('src');
			rightImgSrc = $('.item-large').first().children('img').first().attr('src');
			
			leftTitleValue = $('.item-large').eq($('.item-large').length-2).find('h2').first().text();
			rightTitleValue = $('.item-large').first().find('h2').first().text();
			
			//console.log($('.item-large').eq($('.item-large').length-2).find('h2').first().text());
		}else{
			
			$('.item-large').eq(currItemLarge).hide();
			$('.item-large').eq(currItemLarge-1).show();
			
			if(currItemLarge-2<0){
				leftImgSrc = $('.item-large').last().children('img').first().attr('src');
				leftTitleValue = $('.item-large').last().find('h2').first().text();
			}else{
				leftImgSrc = $('.item-large').eq(currItemLarge-2).children('img').first().attr('src');
				leftTitleValue = $('.item-large').eq(currItemLarge-2).find('h2').first().text();
			}
			
			if(currItemLarge-2>$('.item-large').length-1){
				rightImgSrc = $('.item-large').first().children('img').first().attr('src');
				rightTitleValue = $('.item-large').first().find('h2').first().text();
			}else{
				rightImgSrc = $('.item-large').eq(currItemLarge).children('img').first().attr('src');
				rightTitleValue = $('.item-large').eq(currItemLarge).find('h2').first().text();
			}
		}
		//console.log(leftTitleValue);
		//console.log(rightTitleValue);
		$('.item-small.left').first().children('img').first().attr('src',leftImgSrc);
		$('.item-small.left').first().children('h4').first().text(leftTitleValue);
		$('.item-small.right').first().children('img').first().attr('src',rightImgSrc);
		$('.item-small.right').first().children('h4').first().text(rightTitleValue);
	});
	$('.item-small.right').click(function(){
		var currItemLarge = 0;
		var leftImgSrc = '';
		var rightImgSrc = '';
		var leftTitleValue = '';
		var rightTitleValue = '';
		$('.item-large').each(function(ix,el){
			if(!$(el).is(':hidden')){
				currItemLarge = ix;
			}
		});
		if(currItemLarge==$('.item-large').length-1){
			
			$('.item-large').last().hide();
			$('.item-large').first().show();
			
			leftImgSrc = $('.item-large').eq($('.item-large').length-1).children('img').first().attr('src');
			rightImgSrc = $('.item-large').eq(1).children('img').first().attr('src');
			
			leftTitleValue = $('.item-large').eq($('.item-large').length-1).find('h2').first().text();
			rightTitleValue = $('.item-large').eq(1).find('h2').first().text();
			
		}else{
			$('.item-large').eq(currItemLarge).hide();
			$('.item-large').eq(currItemLarge+1).show();
			
			if(currItemLarge+2>$('.item-large').length-1){
				rightImgSrc = $('.item-large').first().children('img').first().attr('src');
				rightTitleValue = $('.item-large').first().find('h2').first().text();
			}else{
				rightImgSrc = $('.item-large').eq(currItemLarge+2).children('img').first().attr('src');
				rightTitleValue = $('.item-large').eq(currItemLarge+2).find('h2').first().text();
			}
			
			leftImgSrc = $('.item-large').eq(currItemLarge).children('img').first().attr('src');
			leftTitleValue = $('.item-large').eq(currItemLarge).find('h2').first().text();
		}
		
		$('.item-small.left').first().children('img').first().attr('src',leftImgSrc);
		$('.item-small.left').first().children('h4').first().text(leftTitleValue);
		$('.item-small.right').first().children('img').first().attr('src',rightImgSrc);
		$('.item-small.right').first().children('h4').first().text(rightTitleValue);
	});
	
})(window, jQuery);

/*var map = '';

function initialize() {
    var mapOptions = {
      zoom: 14,
      center: new google.maps.LatLng(37.769725, -122.462154)
    };
    map = new google.maps.Map(document.getElementById('map'),  mapOptions);
}

// load google map
var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' +
        'callback=initialize';
    document.body.appendChild(script);*/