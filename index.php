<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <title>Услуги мини-экскаватора</title>
    <meta name="description" content="Здесь можно заказать услуги мини-экскаватора в Курске и Курской области, узнать цены">
	<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
<!--

Sprint Template

http://www.templatemo.com/tm-401-sprint

-->
    <meta name="viewport" content="width=device-width">
	<meta name="yandex-verification" content="55b5b8bd88fd4591" />
    <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet">-->

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/normalize.min.css">
	<link href="https://f.usemind.org/f/1/fas/c.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/templatemo_misc.css">
    <link rel="stylesheet" href="css/templatemo_style.css">
	<link rel="stylesheet" href="css/jquery.fancybox.min.css" >
	<!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">-->
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
	
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript" >
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter48796553 = new Ya.Metrika({
						id:48796553,
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true,
						webvisor:true
					});
				} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = "https://mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/48796553" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
	
	
</head>
<body>
    <!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
    <div id="front">
        <div class="site-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div id="templatemo_logo">
                            <h1><a href="#contact">Услуги <br> мини-экскаватора<br/>89045257770</a></h2>
                        </div> <!-- /.logo -->
                    </div> <!-- /.col-md-4 -->
                    <div class="col-md-9 col-sm-6 col-xs-6">
                        <a href="#" class="toggle-menu"><i class="fa fa-bars"></i></a>
                        <div class="main-menu">
                            <ul>
                                <li><a href="#front">Начало</a></li>
                                <li><a href="#services">Услуги</a></li>								
                                <!--<li><a href="#products">Работы</a></li>-->
								<li><a href="#prices">Цены</a></li>
                                <li><a href="#contact">Контакты</a></li>
                            </ul>
                        </div> <!-- /.main-menu -->
                    </div> <!-- /.col-md-8 -->
                </div> <!-- /.row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="responsive">
                            <div class="main-menu">
                                <ul>
                                    <li><a href="#front">Начало</a></li>
                                    <li><a href="#services">Услуги</a></li>                                    
									<!--<li><a href="#products">Работы</a></li>-->
									<li><a href="#prices">Цены</a></li>
                                    <li><a href="#contact">Контакты</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- /.container -->
        </div> <!-- /.site-header -->
    </div> <!-- /#front -->

    <div class="site-slider">
        <ul class="bxslider">
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="slider-caption">
								<div class="responsive">
                                <h3><a href="#services">Услуги мини-экскаватора в Курске и Курской области</a></h3>
								</div>
							</div>
                        </div>
                    </div>
                </div>
				<img src="<?PHP echo $SERVER['DOCUMENT_ROOT']?>/images/slider/slide1.jpg" alt="slider image 1">
            </li>
            <li>
                <div class="container caption-wrapper">
                    <div class="slider-caption">
                        <h3><a href="#contact">без посредников</a></h3>
                    </div>
                </div>
				<img src="<?PHP echo $SERVER['DOCUMENT_ROOT']?>/images/slider/slide2.jpg" alt="slider image 2">
            </li>
            <li>
				<div class="container">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="slider-caption">
                                <h3>все виды работ</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <img src="<?PHP echo $SERVER['DOCUMENT_ROOT']?>/images/slider/slide3.jpg" alt="slider image 3">
            </li>
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="slider-caption">
                                <h3>Выезжаем в районы</h3>
                            </div>
                        </div>
                    </div>
                </div>
				<img src="<?PHP echo $SERVER['DOCUMENT_ROOT']?>/images/slider/slide4.jpg" alt="slider image 4">
            </li>
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="slider-caption">
                                <h3>Большой опыт работы</h3>
                            </div>
                        </div>
                    </div>
                </div>
				<img src="<?PHP echo $SERVER['DOCUMENT_ROOT']?>/images/slider/slide5.jpg" alt="slider image 5">
            </li>
        </ul> <!-- /.bxslider -->
        <!--<div class="bx-thumbnail-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="bx-pager">
                            <a data-slide-index="0" href=""><img src="images/slider/thumb1.jpg" alt="image 1" /></a>
                            <a data-slide-index="1" href=""><img src="images/slider/thumb2.jpg" alt="image 2" /></a>
                            <a data-slide-index="2" href=""><img src="images/slider/thumb3.jpg" alt="image 3" /></a>
                            <a data-slide-index="3" href=""><img src="images/slider/thumb4.jpg" alt="image 4" /></a>
                            <a data-slide-index="4" href=""><img src="images/slider/thumb5.jpg" alt="image 5" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
    </div> <!-- /.site-slider -->

    <div id="services" class="content-section">
        <div class="container">
			<div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="section-title">Услуги</h2>
                </div> <!-- /.col-md-12 -->
				<div class="col-md-offset-2 col-md-8 text-center bigger-text" style="margin-bottom:0">
								<p>Все виды земляных работ!</p>
						</div>
            </div> <!-- /.row -->
			
			<div class="row">
				<div class="col-md-1 hidden-sm"></div>
				<div class="col-md-4 col-sm-6 col-xs-6" >
					<div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6 col-xs-hidden"><span class="service-icon first"></span></div>
							<div class="col-md-10 col-sm-6">
									<h4>Траншеи</h4>
									<p>Копка траншей под фундамент, газ, воду, кабель, сливные ямы и др.</p>
							</div>
						</div>
					</div>
                </div> <!-- /.col-md-3 -->
				<div class="col-md-3 hidden-sm"></div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6 col-xs-hidden"><span class="service-icon second"></span></div>
							<div class="col-md-10 col-sm-6">
								<h4>Фундаменты</h4>
								<p>Копка фундаментов</p>
							</div>
						</div>
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<!--<div class="col-md-1 hidden-sm"></div>-->
			</div> <!-- /.row -->
            <div class="row">
				<div class="col-md-1 hidden-sm"></div>
				<div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6 col-xs-hidden"> <span class="service-icon third"></span></div>
							<div class="col-md-10 col-sm-6">
								<h4>Сливные ямы</h4>
								<p>Копка сливных ям под канализацию, туалет, септик любого объема и глубины</p>
							</div>
						</div>
                        <!--<p>Sprint is free responsive web template using HTML5 CSS3 and Bootstrap framework. Feel free to download, modify and use it for your site.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<div class="col-md-3 hidden-sm"></div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6 col-xs-hidden"><span class="service-icon fourth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h4>Водоемы</h4>
								<p>Копка бассейнов, прудов и водоемов</p>
							</div>
						</div>
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<!--<div class="col-md-1 hidden-sm"></div>-->
			</div>
			<div class="row">
				<div class="col-md-1 hidden-sm"></div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6 col-xs-hidden"><span class="service-icon fifth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h4>Подвалы</h4>
								<p>Копка подвалов</p>
							</div>
						</div>
                        <!--<p>Копаем траншеи под фундамент,газ,воду,кабель,а также сливные ямы.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<div class="col-md-3 hidden-sm"></div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6 col-xs-hidden"><span class="service-icon sixth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h4>Погреба</h4>
								<p>Копка погребов</p>
							</div>
						</div>
                        <!--<p>Etiam porttitor nibh et felis molestie fermentum. Ut quis diam porttitor, dictum dolor in, volutpat nulla. Phasellus egestas eu lacus eu pharetra.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<!--<div class="col-md-1 hidden-sm"></div>-->
			</div> <!-- /.row -->
			
			<div class="row">
				<div class="col-md-1 hidden-sm"></div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6 col-xs-hidden"><span class="service-icon fifth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h4>Обустройство скважин</h4>
								<p></p>
							</div>
						</div>
                        <!--<p>Копаем траншеи под фундамент,газ,воду,кабель,а также сливные ямы.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<div class="col-md-3 hidden-sm"></div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6 col-xs-hidden"><span class="service-icon sixth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h4>Колодцы</h4>
								<p>Копка колодцев</p>
							</div>
						</div>
                        <!--<p>Etiam porttitor nibh et felis molestie fermentum. Ut quis diam porttitor, dictum dolor in, volutpat nulla. Phasellus egestas eu lacus eu pharetra.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<!--<div class="col-md-1 hidden-sm"></div>-->
			</div> <!-- /.row -->
			
			<div class="row">
				<div class="col-md-1 hidden-sm"></div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6 col-xs-hidden"><span class="service-icon fifth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h4>Котлованы</h4>
								<p>Копка котлованов</p>
							</div>
						</div>
                        <!--<p>Копаем траншеи под фундамент,газ,воду,кабель,а также сливные ямы.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<div class="col-md-3 hidden-sm"></div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6 col-xs-hidden"><span class="service-icon sixth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h4>Закопка траншей</h4>
							</div>
						</div>
                        <!--<p>Etiam porttitor nibh et felis molestie fermentum. Ut quis diam porttitor, dictum dolor in, volutpat nulla. Phasellus egestas eu lacus eu pharetra.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<!--<div class="col-md-1 hidden-sm"></div>-->
			</div> <!-- /.row -->
			
			<div class="row">
				<div class="col-md-1 hidden-sm"></div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6 col-xs-hidden"><span class="service-icon seventh"></span></div>
							<div class="col-md-10 col-sm-6">
								<h4>Планировка территории</h4>
							</div>
						</div>
                        <!--<p>Sprint is free responsive web template using HTML5 CSS3 and Bootstrap framework. Feel free to download, modify and use it for your site.</p>-->
                    </div> <!-- /.service-item -->
                </div> <!-- /.col-md-3 -->
				<div class="col-md-3 hidden-sm"></div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="service-item">
						<div class="row">
							<div class="col-md-2 col-sm-6 col-xs-hidden"><span class="service-icon eighth"></span></div>
							<div class="col-md-10 col-sm-6">
								<h4>Дополнительные услуги</h4>
								<p>Также есть манипулятор стрела 7т и бригада каменщиков: кладут красиво и аккуратно без вредных привычек</p>
							</div>
						</div>
					</div>
                </div> <!-- /.col-md-3 -->
				<!--<div class="col-md-1 hidden-sm"></div>-->
			</div> <!-- /.row -->
			<div class="row">
				<div class="col-md-offset-2 col-md-8 text-center bigger-text">
                    <p>Есть предложения по другим видам работ? <a href="#contact">Свяжитесь с нами!</a></p>
					<p>Работаем на всей территории Курской области</p>
                </div>
			</div>
        </div> <!-- /.container -->
    </div> <!-- /#services -->

	
	<div id="tech" class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="section-title">Технические характеристики</h2>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
            <div class="row">                
				<div class="item-large">
					<div class="col-md-12 col-sm-12">
						<div class="item-header">
							<h3 class="pull-left">Мини-экскаватор на резиновом ходу</h3>
							<!--<span class="pull-right">Rate: <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i></span>-->
							<div class="clearfix"></div>
						</div> <!-- /.item-header -->
					</div>
					<div class="col-md-6 col-sm-6">
						<img src="/images/tech.jpg" alt="Мини-экскаватор технические характеристики">
					</div>
					<div class="tech-content">
						<div class="col-md-6 col-sm-6">
							<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 tech title">Ковши:</div> <div class="col-md-6 col-sm-6 col-xs-6 tech value"><strong>30, 40, 50, 60, 70 (см)</strong></div>
							</div>
							<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 tech title">Глубина копания</div> <div class="col-md-6 col-sm-6 col-xs-6 tech value"><strong>3 м</strong></div>
							</div>
							<div class="row">
							<div class="col-md-12 col-sm-12 tech title" style="text-align: center; margin-top: 15px;">Габаритные размеры экскаватора:</div>
							</div>
							<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 tech title">ширина</div> <div class="col-md-6 col-sm-6 col-xs-6 tech value"><strong>1,6 м</strong></div>
							</div>
							<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 tech title">высота</div> <div class="col-md-6 col-sm-6 col-xs-6 tech value"><strong>2,6 м</strong></div>
							</div>
							<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 tech title">длина стрелы</div> <div class="col-md-6 col-sm-6 col-xs-6 tech value"><strong>3,5 м</strong></div>
							</div>
							<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 tech title">планировочный нож</div> <div class="col-md-6 col-sm-6 col-xs-6 tech value"><strong>1,6 м</strong></div>
							</div>
							<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6 tech title">Вес:</div> <div class="col-md-6 col-sm-6 col-xs-6 tech value"><strong>3000 кг</strong></div>
							</div>
						</div>
					</div>
				</div> <!-- /.item-large -->
            </div> <!-- /.row -->
			<div class="row">
				<div class="col-md-offset-2 col-md-8 text-center bigger-text">
                    <p>Экскаватор может работать в труднодоступных местах.</p>
					<p>Экскаватор - на резиновых гусеницах, что не позволит испортить тротуарную плитку или асфальт!</p>
					<p>Работу выполняет мастер с большим опытом.</p>
                </div>
			</div>
        </div> <!-- /.container -->
    </div> <!-- /#products -->
	
    <div id="products" class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="section-title">Примеры работ</h2>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_1.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div>--> <!-- /.overlay -->
                            <img src="/images/products/product1.jpg" alt="">
                        </div> <!-- /.item-thumb -->
                        <!--<h4>Технические пояснения</h4>-->
                        <!--<span>Price: <em class="text-muted">$260.00</em> - <em class="price">$180.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_3.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div> --><!-- /.overlay -->
							<a data-fancybox="gallery" href="/images/products/product2.jpg">
								<img src="/images/products/product2.jpg" alt="">
							</a>
                        </div> <!-- /.item-thumb -->
                        <!--<h4>Технические пояснения</h4>-->
                        <!--<span>Price: <em class="text-muted">$320.00</em> - <em class="price">$240.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_2.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div> --><!-- /.overlay -->
                            <a data-fancybox="gallery" href="/images/products/product3.jpg">
								<img src="/images/products/product3.jpg" alt="">
							</a>
							
                        </div> <!-- /.item-thumb -->
                        <!--<h4>Технические пояснения</h4>-->
                        <!--<span>Price: <em class="text-muted">$480.00</em> - <em class="price">$340.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_1.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div> --><!-- /.overlay -->
                            <a data-fancybox="gallery" href="/images/products/product4.jpg">
								<img src="/images/products/product4.jpg" alt="">
							</a>
                        </div> <!-- /.item-thumb -->
                        <!--<h4>Технические пояснения</h4>-->
                        <!--<span>Price: <em class="text-muted">$260.00</em> - <em class="price">$140.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_3.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div> --><!-- /.overlay -->
							<a data-fancybox="gallery" href="/images/products/product5.jpg">
								<img src="/images/products/product5.jpg" alt="">
							</a>
                        </div> <!-- /.item-thumb -->
                        <!--<h4>Технические пояснения</h4>-->
                        <!--<span>Price: <em class="text-muted">$400.00</em> - <em class="price">$260.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_1.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div> --><!-- /.overlay -->
							<a data-fancybox="gallery" href="/images/products/product6.jpg">
								<img src="/images/products/product6.jpg" alt="">
							</a>
                        </div> <!-- /.item-thumb -->
                        <!--<h4>Технические пояснения</h4>-->
                        <!--<span>Price: <em class="text-muted">$470.00</em> - <em class="price">$330.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_2.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div> --><!-- /.overlay -->
							<a data-fancybox="gallery" href="/images/products/product7.jpg">
								<img src="/images/products/product7.jpg" alt="">
							</a>
                        </div> <!-- /.item-thumb -->
                        <!--<h4>Технические пояснения</h4>-->
                        <!--<span>Price: <em class="text-muted">$680.00</em> - <em class="price">$480.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <div class="item-thumb">
                            <!--<span class="note"><img src="images/small_logo_3.png" alt=""></span>-->
                            <!--<div class="overlay">
                                <div class="overlay-inner">
                                    <a href="#nogo" class="view-detail">Add to Cart</a>
                                </div>
                            </div> --><!-- /.overlay -->
							<a data-fancybox="gallery" href="/images/products/product8.jpg">
								<img src="/images/products/product8.jpg" alt="">
							</a>
                        </div> <!-- /.item-thumb -->
                        <!--<h4>Технические пояснения</h4>-->
                        <!--<span>Price: <em class="text-muted">$820.00</em> - <em class="price">$660.00</em></span>-->
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /#products -->
	
	
	 <div id="prices" class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="section-title">Цены</h2>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
            <div class="row">
                <div class="col-md-2 hidden-sm"></div>
				<div class="col-md-3 col-sm-6 col-xs-6">
					<h5>Один метр траншеи</h5>
				</div>
				<div class="col-md-2 hidden-sm"></div>
				<div class="col-md-3 col-sm-6 col-xs-6">
					<h4>150 р.</h4>
				</div>
				<div class="col-md-2 hidden-sm"></div>
            </div> <!-- /.row -->
			<div class="row">
				<div class="col-md-2 hidden-sm"></div>
                <div class="col-md-3 col-sm-6 col-xs-6">
					<h5>Один кубический метр ямы</h5>
				</div>
				<div class="col-md-2 hidden-sm"></div>
				<div class="col-md-3 col-sm-6 col-xs-6">
					<h4>250 р.</h4>
				</div>
				<div class="col-md-2 hidden-sm"></div>
            </div> <!-- /.row -->
			<div class="row">
				<div class="col-md-2 hidden-sm"></div>
                <div class="col-md-3 col-sm-6 col-xs-6">
					<h5>Планировочные работы</h5>
				</div>
				<div class="col-md-2 hidden-sm"></div>
				<div class="col-md-3 col-sm-6 col-xs-6">
					<h4>1500 р. в час</h4>
				</div>
				<div class="col-md-2 hidden-sm"></div>
            </div> <!-- /.row -->
			<div class="col-md-offset-0 col-md-12 text-center bigger-text">
                    <p>Стоимость минимального заказа - <span class="price-value">4500 р.</span> Услуги и топливо входят в стоимость заказа.</p>
            </div>
        </div> <!-- /.container -->
    </div> <!-- /#products -->
	
	
    <div id="contact" class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="section-title">Контакты</h2>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center bigger-text">
                    <p>Меня зовут Виталий. Звоните в любое время или присылайте сообщения на электронный почтовый ящик</p>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div id="phone"><span>Телефон:</span><h3><a href="tel:89045257770">89045257770</a></h3></div>
					<div id="email"><span>Электронная почта:</span><h3><a href="mailto:kursk-ekskavator@yandex.ru">kursk-ekskavator@yandex.ru</a></h3></div>
					<!--<div id="map">
                    </div>-->
                </div> <!-- /.col-md-6 -->
                <div class="col-md-6 col-sm-6">
                    <form action= "post.php" method= "POST"> 
                    <div class="row contact-form">
						<? if($_GET['FormErrors']==='0'):?>
						<fieldset class="col-md-12">
						   <p style="color: green;font-size:14px;font-weight:bolder;">Сообщение успешно отправлено!</p>
                        </fieldset>
						<?elseif ($_GET['FormErrors']==='1'):?>
						<fieldset class="col-md-12">
						   <p style="color: red;font-size:14px;font-weight:bolder;">Сообщение не отправлено! Проверьте введенные данные.</p>
                        </fieldset>
						<?endif;?>
                        <fieldset class="col-md-6 col-sm-6">
                            <input id="name" type="text" name="name" placeholder="Имя">
                        </fieldset>
                        <fieldset class="col-md-6 col-sm-6">
                            <input type="email" name="email" id="email" placeholder="Email">
                        </fieldset>
						<fieldset class="col-md-6 col-sm-6">
                            <input type="phone" name="phone" id="phone" pattern="^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$" placeholder="Телефон">
                        </fieldset>
                        <fieldset class="col-md-12">
                            <input type="text" name="subject" id="subject" placeholder="Тема">
                        </fieldset>
                        <fieldset class="col-md-12">
                            <textarea name="comments" id="comments" placeholder="Сообщение"></textarea>
                        </fieldset>
                        <fieldset class="col-md-12">
                            <input type="submit" name="send" value="Отправить" id="submit" class="button">
                        </fieldset>
                    </div> <!-- /.contact-form -->
                    </form>
                </div> <!-- /.col-md-6 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /#products -->

    <div class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-6">
                    <span id="copyright">
                    	Copyright &copy; 2018 <a href="mailto:kiischivarino@yandex.ru">Created by Kiis-Chivarino</a>
                    </span>

            </div> <!-- /.col-md-6 -->
                <div class="col-md-4 col-sm-6">
                    <ul class="social">
						<li><a href="https://vk.com/kurskekskavator" target="_blank" class="fa-vk"></a></li>
                        <li><a href="https://www.facebook.com/kurskekskavator" target="_blank" class="fa fa-facebook"></a></li>
                        <!--<li><a href="#" class="fa fa-twitter"></a></li>
                        <li><a href="#" class="fa fa-instagram"></a></li>
                        <li><a href="#" class="fa fa-linkedin"></a></li>
                        <li><a href="#" class="fa fa-rss"></a></li>-->
                    </ul>
                </div> <!-- /.col-md-6 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.site-footer -->

    
   <!--<script src="js/vendor/jquery-1.10.1.min.js"></script>
   <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
   -->
   <script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
   
	<script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing-1.3.js"></script>
	<script src="js/jquery.easing.js"></script>
    <script src="js/bootstrap.js"></script>
	<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    <!-- templatemo 401 sprint -->
</body>
</html>